// @generated automatically by Diesel CLI.

diesel::table! {
    committee (id) {
        id -> Int4,
        name -> Text,
        description -> Text,
        date_added -> Timestamp,
    }
}

diesel::table! {
    committee_member (id) {
        id -> Int4,
        member_id -> Int4,
        committee_id -> Int4,
        date_added -> Timestamp,
    }
}

diesel::table! {
    member (id) {
        id -> Int4,
        discord_id -> Int8,
        name -> Text,
        date_added -> Timestamp,
    }
}

diesel::table! {
    project (id) {
        id -> Int4,
        name -> Text,
        description -> Text,
        date_added -> Timestamp,
    }
}

diesel::table! {
    project_member (id) {
        id -> Int4,
        member_id -> Nullable<Int4>,
        project_id -> Nullable<Int4>,
        date_added -> Timestamp,
    }
}

diesel::joinable!(committee_member -> committee (committee_id));
diesel::joinable!(committee_member -> member (member_id));
diesel::joinable!(project_member -> member (member_id));
diesel::joinable!(project_member -> project (project_id));

diesel::allow_tables_to_appear_in_same_query!(
    committee,
    committee_member,
    member,
    project,
    project_member,
);
