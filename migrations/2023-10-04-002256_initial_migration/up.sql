create table member
(
    id         serial
        constraint member_pk
            primary key,
    discord_id bigint                  not null
        constraint member_discord
            unique,
    name       text                    not null,
    date_added timestamp default now() not null
);

comment on column member.name is 'Student actual name';

create table committee
(
    id          serial
        constraint committee_pk
            primary key,
    name        text                    not null,
    description text                    not null,
    date_added  timestamp default now() not null
);

create table committee_member
(
    id           serial
        constraint committee_member_pk
            primary key,
    member_id    integer                 not null
        constraint member_fk
            references member(id)
            on delete cascade ,
    committee_id integer                 not null
        constraint committee_fk
            references committee(id)
            on delete cascade ,
    date_added   timestamp default now() not null
);

create unique index committee_member_uindex
    on committee_member (committee_id, member_id);

create table project
(
    id          serial
        constraint project_pk
            primary key,
    name        text                    not null,
    description text                    not null,
    date_added  timestamp default now() not null
);

create table project_member
(
    id         serial
        constraint project_member_pk
            primary key,
    member_id  integer
        constraint member_fk
            references member(id)
            on delete cascade ,
    project_id integer
        constraint project_fk
            references project(id)
            on delete cascade ,
    date_added timestamp default now() not null
);



